'use strict';
const airportsAPI = 'https://eng1003.monash/OpenFlights/airports/';
const routesAPI = 'https://eng1003.monash/OpenFlights/routes/';
const allRoutesAPI = 'https://eng1003.monash/OpenFlights/allroutes/';

function httpGet(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) callback(JSON.parse(xmlHttp.responseText));
    };
    xmlHttp.open('GET', theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}
function getRoutes(airportId, callback) {
    webServiceRequest(routesAPI, { sourceAirport: airportId, callback: callback });
}
function getAirports(country, callback) {
    webServiceRequest(airportsAPI, { country: country, callback: callback });
}
function webServiceRequest(url, data) {
    // Build URL parameters from data object.
    let params = '';
    // For each key in data object...
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            if (params.length == 0) {
                // First parameter starts with '?'
                params += '?';
            } else {
                // Subsequent parameter separated by '&'
                params += '&';
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + '=' + encodedValue;
        }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}
