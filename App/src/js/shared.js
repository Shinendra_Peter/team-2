'use strict';
const APP_PREFIX = 'TravelApp';
const AIRPORT_LAYER_OPTIONS = {
    'icon-image': 'airport-15',
    'icon-allow-overlap': true,
    'icon-ignore-placement': true,
    'text-field': ['get', 'name'], // get the title name from the source's "title" property
    'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
    'text-offset': [0, 0.6],
    'text-anchor': 'top',
    'text-allow-overlap': true,
    'text-size': 10,
};
mapboxgl.accessToken = 'pk.eyJ1IjoiYXZpc2hrYTEydGciLCJhIjoiY2tqdHpya2J1MXY3YjJybDFjNW43bXRxeiJ9.sIsULsyaZXFNwEsJAKqPwg';

class Trip {
    _creationDate;
    _tripDate;
    _tripTime;
    _schedule;
    _tripName;
    _country;
    constructor(name) {
        this._tripName = name;
        this._creationDate = new Date().getTime();
        this._tripDate = '';
        this._country = '';
        this._schedule = [];
    }

    setCountry(country) {
        this._country = country;
    }
    setDateTime(date, time) {
        this._tripDate = date;
        this._tripTime = time;
    }
    setSchedule(routes) {
        this._schedule = routes;
    }

    getDate() {
        return this._tripDate;
    }
    getCountry() {
        return this._country;
    }
    getName() {
        return this._tripName;
    }
    getSchedule(routes) {
        return this._schedule;
    }
    getCreatedDate() {
        return new Date(this._creationDate).toISOString();
    }
    canCancel() {
        let tripDate = new Date(this._tripDate);
        let now = new Date();
        let today = new Date(now.toISOString().substring(0, 10));
        if (tripDate.getTime() > today.getTime()) {
            return true;
        }
        return false;
    }
    getSummary() {
        const summary = {
            name: this._tripName,
            date: this._tripDate,
            time: this._tripTime,
            origin: this._schedule[0].properties.sourceAirport,
            destination: this._schedule[this._schedule.length - 1].properties.destinationAirport,
            stops: this._schedule.length + 1,
        };
        return summary;
    }

    fromData(data) {
        this._tripName = data._tripName;
        this._creationDate = data._creationDate;
        this._country = data._country;
        this._tripDate = data._tripDate;
        this._tripTime = data._tripTime;
        this._schedule = data._schedule;
    }
}

class TripList {
    _allTrips;
    constructor() {
        const savedTrips = getData('TripList');
        if (savedTrips !== null) {
            const converted = [];
            savedTrips.forEach((trip) => {
                let t = new Trip('name');
                t.fromData(trip);
                converted.push(t);
            });
            this._allTrips = converted;
        } else {
            this._allTrips = [];
        }
    }

    getAllTrips() {
        return this._allTrips;
    }

    getCount() {
        return this._allTrips.length;
    }

    getTrip(index) {
        return this._allTrips[index];
    }

    addTrip(trip) {
        this._allTrips.push(trip);
        saveData('TripList', this._allTrips);
    }

    removeTrip(index) {
        this._allTrips.splice(index, 1);
        saveData('TripList', this._allTrips);
    }
}

function makePoint(coordinates, properties) {
    return {
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: coordinates,
        },
        properties: properties,
    };
}
function makeMapSource(data) {
    return {
        type: 'geojson',
        cluster: false,
        data: { type: 'FeatureCollection', features: data },
    };
}

function getData(key) {
    return JSON.parse(localStorage.getItem(APP_PREFIX + key));
}
function saveData(key, data) {
    localStorage.setItem(APP_PREFIX + key, JSON.stringify(data));
}

function centerMap(res) {
    // zoom and center map to fit a given feature
    map.easeTo({
        center: res.features[0].center,
        zoom: 9,
    });
    map.fitBounds(res.features[0].bbox);
}

let tripList = new TripList(); // maintain a list of trips at all instances
