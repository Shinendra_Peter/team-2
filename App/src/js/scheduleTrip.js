'use strict';
// read back data from local storage
const tripDetails = JSON.parse(localStorage.getItem(APP_PREFIX + 'trip'));
const journey = new Trip();
journey.fromData(tripDetails);

const country = journey.getCountry();
const airportsInCountry = [];
let routes = [];
const selectedRoutes = [];
const COUNTRY_URL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + country + '.json?&access_token=' + mapboxgl.accessToken;

var map = new mapboxgl.Map({
    container: 'mapbox-map',
    style: 'mapbox://styles/mapbox/streets-v11',
});

function showAirports(airports) {
    //Add markers for each airport
    const markers = [];
    airports.forEach((airport) => {
        updateAirportList(airport);
        markers.push(makePoint([airport.longitude, airport.latitude], airport));
    });
    let airportLocations = {
        type: 'FeatureCollection',
        features: markers,
    };
    map.getSource('airport-locations').setData(airportLocations);
}

function showRoutes(routeData) {
    //create lines for each allowed route
    routes = [];
    let routeId = 0;
    routeData.forEach((route) => {
        if (airportsInCountry.find((el) => el.airportId === route.destinationAirportId.toString())) {
            routes.push({
                type: 'Feature',
                geometry: {
                    type: 'LineString',
                    coordinates: [getAirportLocation(route.sourceAirportId), getAirportLocation(route.destinationAirportId)],
                },
                properties: { routeId: routeId, ...route },
            });
            routeId += 1;
        }
    });

    let routePaths = {
        type: 'FeatureCollection',
        features: routes,
    };
    map.getSource('airport-routes').setData(routePaths); // add route lines to map
}

function updateSelectedRoutes(selectedRoute) {
    if (selectedRoute) {
        selectedRoutes.push(selectedRoute);
    }
    const routeLines = {
        type: 'FeatureCollection',
        features: selectedRoutes,
    };
    map.getSource('selected-routes').setData(routeLines);
}

function updateAirportList(airport) {
    if (!airportsInCountry.find((el) => el.airportId === airport.airportId)) {
        airportsInCountry.push(airport);
    }
}

function getAirportLocation(airportId) {
    let airport = airportsInCountry.find((el) => el.airportId === airportId.toString());
    if (airport) {
        return [airport.longitude, airport.latitude];
    }
}

function undo() {
    // remove last selected Route
    const removedRoute = selectedRoutes.pop();
    if (removedRoute == undefined) {
        showRoutes([]); //reset source selection
        return;
    }
    updateSelectedRoutes();
    getRoutes(removedRoute.properties.sourceAirportId, 'showRoutes');
}

function saveTrip() {
    journey.setSchedule(selectedRoutes);
    let details = journey.getSummary();
    if (confirm('Confirm Trip? \n Summary:' + `Date: ${details.date} | Time : ${details.time} |  Origin: ${details.origin} | Destination: ${details.destination} | Stop: ${details.stops}`)) {
        tripList.addTrip(journey);
        localStorage.setItem(APP_PREFIX + 'selectedTrip', tripList.getCount() - 1);
        window.location = './tripDetails.html';
    } else {
        window.location = './index.html';
    }
}

map.on('load', function () {
    map.addSource('airport-locations', makeMapSource([]));

    map.addSource('airport-routes', makeMapSource([]));

    map.addSource('selected-routes', makeMapSource([]));

    map.addLayer({
        // Add map layer to show airport locations
        id: 'airport-locations',
        type: 'symbol',
        source: 'airport-locations',
        layout: AIRPORT_LAYER_OPTIONS,
    });

    map.addLayer({
        // Add map layer to show routes from selected airport
        id: 'airport-routes',
        type: 'line',
        source: 'airport-routes',
        layout: {},
        paint: {
            'line-color': 'blue',
            'line-width': 8,
        },
    });

    map.addLayer({
        // Add map layer to show selected routes
        id: 'selected-routes',
        type: 'line',
        source: 'selected-routes',
        layout: {},
        paint: {
            'line-color': 'red',
            'line-width': 4,
        },
    });

    map.on('click', 'airport-locations', (e) => {
        // load and display domestic routes from clicked airport
        if (routes.length === 0) {
            const airportId = e.features[0].properties.airportId;
            getRoutes(airportId, 'showRoutes');
        }
    });

    map.on('click', 'airport-routes', (e) => {
        // save selected route and display selected routes for destination airport
        const selectedRoute = routes.find((el) => el.properties.routeId === e.features[0].properties.routeId);
        updateSelectedRoutes(selectedRoute);
        getRoutes(selectedRoute.properties.destinationAirportId, 'showRoutes');
    });

    httpGet(COUNTRY_URL, centerMap); // move and center map over country
    getAirports(country, 'showAirports'); // get airports in the country and display on map
});
