'use strict';

let index = JSON.parse(localStorage.getItem(APP_PREFIX + 'selectedTrip'));
if (index == null || index === '' || index == undefined) {
    window.location = './index.html'; // redirect to home if no index
}

const trip = tripList.getTrip(index);
const country = trip.getCountry();
const airportsInCountry = [];
const routes = trip.getSchedule();
const selectedRoutes = [];
const COUNTRY_URL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + country + '.json?&access_token=' + mapboxgl.accessToken;

var map = new mapboxgl.Map({
    container: 'mapbox-map',
    style: 'mapbox://styles/mapbox/streets-v11',
});

if (trip.canCancel()) {
    // show cancel button if trip is cancellable
    document.getElementById('removeBtn').innerHTML = `<button onclick="remove()" class="mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons">delete</i>
    </button>`;
}

// populate trip details
let details = `<h4>Name: ${trip.getName()}</h4><div>Date: ${trip.getDate()}</div><div> Journey : ${routes[0].properties.sourceAirport}`;
routes.forEach((route) => {
    details += ` => ${route.properties.destinationAirport}`;
});
details += ` </div><div> Created on : ${trip.getCreatedDate()}</div>`;
document.getElementById('details').innerHTML = details;
function showAirports(routes) {
    const markers = [];
    markers.push(
        makePoint(routes[0].geometry.coordinates[0], {
            name: routes[0].properties.sourceAirport,
        })
    );
    routes.forEach((route) => {
        markers.push(
            makePoint(route.geometry.coordinates[1], {
                name: route.properties.destinationAirport,
            })
        );
    });
    let airportLocations = {
        type: 'FeatureCollection',
        features: markers,
    };
    map.getSource('airport-locations').setData(airportLocations);
}

function remove() {
    if (confirm('Delete this Trip?')) {
        tripList.removeTrip(index);
        window.location = './index.html';
    }
}

map.on('load', function () {
    map.addSource('airport-locations', makeMapSource([]));

    map.addSource('airport-routes', makeMapSource(routes));

    map.addLayer({
        // Add map layer to show airport locations
        id: 'airport-locations',
        type: 'symbol',
        source: 'airport-locations',
        layout: AIRPORT_LAYER_OPTIONS,
    });

    map.addLayer({
        // Add map layer to show routes from selected airport
        id: 'airport-routes',
        type: 'line',
        source: 'airport-routes',
        layout: {},
        paint: {
            'line-color': 'red',
            'line-width': 4,
        },
    });

    httpGet(COUNTRY_URL, centerMap); // move and center map over country
    showAirports(routes);
});
