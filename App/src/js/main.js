'use strict';
const DEFAULT_COUNTRY = countryData[0];
let dropDown = document.getElementById('country');

for (let i = 0; i < countryData.length; i++) {
    let el = document.createElement('option');
    el.value = countryData[i];
    el.textContent = countryData[i];
    dropDown.appendChild(el);
}
function isFilled(element) {
    if (element.value == '') {
        alert(element.getAttribute('id') + ' is empty');
        return false;
    } else {
        return true;
    }
}
function createTrip() {
    let date = document.getElementById('date');
    let time = document.getElementById('time');
    let country = document.getElementById('country');
    let name = document.getElementById('name');

    if (isFilled(name) && isFilled(date) && isFilled(country) && isFilled(time)) {
        if (confirm('Plan a trip to ' + country.value + ' on ' + date.value + ' ?')) {
            let trip = new Trip(name.value);
            trip.setCountry(country.value);
            trip.setDateTime(date.value, time.value);
            localStorage.setItem(APP_PREFIX + 'trip', JSON.stringify(trip));
            window.location = './scheduleTrip.html';
        }
    }
}

function view(index) {
    localStorage.setItem(APP_PREFIX + 'selectedTrip', index);
    window.location = './tripDetails.html';
}

let tripListHTML = '';
let trips = tripList.getAllTrips();
for (let i = 0; i < trips.length; i++) {
    let trip = trips[i];
    let details = trip.getSummary();
    tripListHTML += `<div class="mdl-cell mdl-cell--6-col">
    <div class=" mdl-shadow--2dp ${trip.canCancel() ? 'future' : 'past'}">
        <div class="mdl-card__title mdl-card--expand ">
            <h2>${details.name}</h2>
        </div>
        <div class="mdl-card__supporting-text mdl-card--expand">
            Date: ${details.date} | Time : ${details.time} <br>
            Origin: ${details.origin}<br>
            Destination: ${details.destination} <br>
            Stop: ${details.stops}
        </div>
        <div class="mdl-card__actions mdl-card--border">
            <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="view(${i})">View Details</a>
            
            ${trip.canCancel() ? '' : '<a class="mdl-button mdl-button mdl-js-button mdl-js-ripple-effect" style="float:right">Past Trip</a>'}
            <div class="mdl-layout-spacer"></div>
        </div>
    </div>
</div>`;
}

document.getElementById('trip-list').innerHTML = tripListHTML;
